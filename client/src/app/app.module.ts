import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { RouterModule, Resolve , Router  } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BackendTestComponent } from './backend-test/backend-test.component';
declare function require(name: string): string;


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BackendTestComponent
  ],
  imports: [
    RouterModule.forRoot([
      {
        path: 'frontend',
        component: HomeComponent
      },
      {
        path: 'backend',
        component: BackendTestComponent
      }
      // {
      //   path: '**',
      //   redirectTo: '/home',
      //   pathMatch: 'full'
      // }

    ]),
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
