import { Component, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { AppService } from '../app.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-backend-test',
  templateUrl: './backend-test.component.html',
  styleUrls: ['./backend-test.component.css']
})
export class BackendTestComponent implements OnInit {

  list: any[] = [];
  joketext: string = "";
  id: number = 0;
  text: string = "";
  likeClick: boolean = false;
  likeFlag: boolean;
  // like: string = "like";
  // Swal = require('sweetalert2');
  test: boolean;
  test2: string;

  constructor(
    private appService: AppService,
    public el: ElementRef,
    public renderer: Renderer2
  ) { }


  backend_getAllJoke() {
    this.list = [];
    this.appService.backend_getAllJoke().subscribe(response => {
      console.log(response);

      for (let i = 0; i < response.length; i++) {
        this.list.push({
          id: response[i].id,
          data: response[i].joke,
          like: response[i].like,
          dislike: response[i].dislike
        });
      }
      console.log(this.list);
    });
  }

  backend_getJokeByID() {
    this.list = [];

      this.appService.backend_getJokeByID(this.id).subscribe(response => {
        console.log(response);
          this.list.push({
            id: response[0].id,
            data: response[0].joke,
            like: response[0].like,
            dislike: response[0].dislike
          });
          console.log(this.list);
          // this.text = response.value.joke;

        
      });
  }

  backend_addNewJoke() {

    // this.list = [];
      console.log("this.text = "+this.joketext);
      this.appService.backend_addNewJoke(this.joketext).subscribe(response => {
        console.log("response = "+JSON.stringify(response));

        if(response.success) {
          Swal({
            position: 'center',
            type: 'success',
            title: 'Add New Joke Complete',
            showConfirmButton: false,
            timer: 1500
          })
          this.backend_getAllJoke();
        }

        
      });
  }

  backend_deleteJoke(id) {

    Swal({
      title: 'Are you sure?',
      text: 'Delete?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {


        this.appService.backend_deleteJoke(id).subscribe(response => {
          console.log("response = "+JSON.stringify(response));
    
          if(response.success) {

            Swal(
              'Deleted!',
              'Your Joke has been delete',
              'success'
            )
            this.backend_getAllJoke();
          }
        });
        
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal(
          'Cancelled',
          '',
          'error'
        )
      }
    })
  }

  like(id, like) {

    this.likeFlag = !like;
    console.log("this.likeFlag = "+this.likeFlag);
    // this.setBackgroundColor(this.likeFlag);
    // this.setFontColor(this.likeFlag);
    console.log("id = "+id);
    console.log("like = "+like);
      this.appService.backend_like(id, like).subscribe(response => {
        console.log("response = "+JSON.stringify(response));
        
      });

      
  }

  setLikeBackgroundColor(like) {
    like = !like;
    // console.log(like);
    if(!like){
      console.log("Blue");
      return "#1E90FF";
    }
    else {
      console.log("White");
      return "white";
    }
  }

  setLikeFontColor(like) {
    like = !like;
    if(!like){
      
      return "white";
    }
    else {
      return "#1E90FF";
    }
  }

  setDislikeBackgroundColor(dislike) {
    if(dislike){
      return "red";
    }
    else {
      return "white";
    }
  }

  setDislikeFontColor(dislike) {
    if(dislike){
      return "white";
    }
    else {
      return "red";
    }
  }

  ngOnInit() {
  }

  click_like(data) {
    data.like++;
  }

  click_dislike(data) {
    data.dislike++;
  }

}
