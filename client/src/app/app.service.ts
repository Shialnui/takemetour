import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AppService {

  constructor(private http: Http) { }

//   authenication(id, password) {
//     return this.http.post('http://localhost:3000/login', {
//       'username': id,
//       'password': password
//     })
//       .map(res => res.json());
//   }

//Front-End
  getAllJoke() {
    return this.http.get(`http://api.icndb.com/jokes`)
      .map(res => res.json());
  }

  getRandomJoke() {
    return this.http.get(`http://api.icndb.com/jokes/random`)
      .map(res => res.json());
  }

  getjokeById(id) {
    return this.http.get(`http://api.icndb.com/jokes/${id}`)
      .map(res => res.json());
  }

  getjokeByName(name,surname) {
    return this.http.get(`http://api.icndb.com/jokes/random?firstName=${name}&&lastName=${surname}`)
      .map(res => res.json());
  }

//Back-End

  backend_getAllJoke() {
    return this.http.get(`http://localhost:3000/getall`)
      .map(res => res.json());
  }

  backend_getJokeByID(id) {
    return this.http.get(`http://localhost:3000/joke/${id}`)
      .map(res => res.json());
  }

  backend_addNewJoke(text) {
    return this.http.post(`http://localhost:3000/addNewJoke`, {
      'text': text
    })
      .map(res => res.json());
  }

  backend_deleteJoke(id) {
    console.log("service id = "+id);
    return this.http.delete(`http://localhost:3000/deleteJoke/${id}`)
      .map(res => res.json());
  }

  backend_like(id, like) {
    return this.http.post(`http://localhost:3000/likeJoke`, {
      'id': id,
      'like': like
    })
      .map(res => res.json());
  }
  

}
