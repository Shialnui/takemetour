import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
// import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  list: any[] = [];
  firstname: string = "";
  lastname: string = "";
  number: number = 0;
  text: string = "";
  img = require("../../assets/chuck-norris.jpg");
  box = require("../../assets/cloud-text.png");

  constructor(
    private appService: AppService
  ) { }

  getAllJoke() {

    if(this.firstname != "" && this.lastname != "") {
      this.list = [];

      this.appService.getjokeByName(this.firstname, this.lastname).subscribe(response => {
        console.log(response);
          this.list.push({
            data: response.value
          });
          console.log(this.list);
          this.text = response.value.joke;

        
      });

    }
    else if(this.number != 0) {
      this.list = [];

      this.appService.getjokeById(this.number).subscribe(response => {
        console.log(response);
          this.list.push({
            data: response.value
          });
          console.log(this.list);
          this.text = response.value.joke;

        
      });
    }
    else {
      this.list = [];

      this.appService.getAllJoke().subscribe(response => {
        console.log(response.value.length);

        for (let i = 0; i < response.value.length; i++) {
          this.list.push({
            data: response.value[i]
          });
        }
        console.log(this.list);
      });
    }
  }

  getRandomJoke() {
    this.list = [];
    this.appService.getRandomJoke().subscribe(response => {
      console.log(response.value.joke);
        this.list.push({
          data: response.value
        });
        this.text = response.value.joke;
    });
  }


  ngOnInit() {
  }

}
