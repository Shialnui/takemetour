var express = require('express');
var app = express();
var cors = require('cors');
var jokeDB = require('./joke.json');
var MongoClient = require('mongodb').MongoClient;
var DBurl = 'mongodb+srv://test001:test001@cluster0-5x2uf.mongodb.net/admin';
// var client = new MongoClient(DBurl, { useNewUrlParser: true });

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());


MongoClient.connect(DBurl, function(err, client) {
    if(err) {
         console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
    }
    console.log('Connected...');
    const collection = client.db("testdb").collection("joke");
    client.close();
 });


app.get('/getall', (req, res) => {
    MongoClient.connect(DBurl, function(err, client) {

        var collection = client.db("testdb").collection("joke");
        collection.find({}).toArray(function (err, data) {
                console.log(data);
                res.send(data);
            });
        client.close();
    });
})

//Get joke by id.
app.get('/joke/:id', (req, res) => {
    MongoClient.connect(DBurl, function(err, client) {
        console.log("Get Joke By ID");
        console.log("id = "+req.params.id);
        var collection = client.db("testdb").collection("joke");
        collection.find({"id": parseInt(req.params.id)}).toArray(function (err, data) {
            console.log(data);
            res.send(data);
        });
        client.close();
    });
})

//Add new joke.
app.post('/addNewJoke', (req, res) => {
    console.log("addNewJoke");
    console.log("text = "+req.body.text);

    MongoClient.connect(DBurl, function(err, client) {

        var id;
        var collection = client.db("testdb").collection("joke");
        collection.find({}).sort({_id:-1}).toArray(function (err, data) {
                console.log(data);
                id = data[0].id;

                MongoClient.connect(DBurl, function(err, client) {
                    console.log("id = "+id);

                    var collection = client.db("testdb").collection("joke");
                    collection.insert({
                        "id": parseInt(id)+1,
                        "joke" : req.body.text,
                        "like": 0,
                        "dislike": 0
                    });

                    res.send({"success": true})

                    client.close();
                });
            });
        client.close();
    });
})

//Delete joke. (In case you hate it)
app.delete('/deleteJoke/:id', (req, res) => {
    console.log("DELETE Joke");
    var id = req.params.id;
    console.log(id);

    MongoClient.connect(DBurl, function(err, client) {
        var collection = client.db("testdb").collection("joke");

        collection.remove({
            "id": parseInt(id)
        });






        res.send({"success": true})

        client.close();
    });
})


app.post('/likeJoke', (req, res) => {
    console.log("like & dislike");
    console.log("id = "+req.body.id);
    console.log("like = "+req.body.like);

    if(req.body.like) {
        req.body.like = false;
    }
    else {
        req.body.like = true;
    }

    MongoClient.connect(DBurl, function(err, client) {

        var collection = client.db("testdb").collection("joke");
        collection.update(
            { "id": req.body.id },
            { 
                "id": req.body.id,
                "joke": 1,
                "like": req.body.like },
            { upsert: true }
        );

        res.send({"success": true})

        client.close();
    });
})






app.listen(3000, () => {
  console.log('Start server at port 3000.')
})