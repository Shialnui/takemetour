var express = require('express');
var app = express();
var cors = require('cors');
var MongoClient = require('mongodb').MongoClient;
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

var uri = "mongodb+srv://test001:test001@testtakemetour-o0arl.mongodb.net/TestTakeMeTour";


app.get('/getall', (req, res) => {
    MongoClient.connect(uri, function(err, client) {
        if(err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
        }
        console.log('Connected...');
        const collection = client.db("TestTakeMeTour").collection("jokedb");
        collection.find({}).toArray(function (err, data) {
                console.log(data);
                res.send(data);
            });
        client.close();
    });
})


app.listen(3009, () => {
    console.log('Start server at port 3009.')
  })